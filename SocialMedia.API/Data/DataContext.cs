using Microsoft.EntityFrameworkCore;
using Models;
using SocialMedia.API.Models;

namespace Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Value> Values {get; set; }
        public DbSet<User> Users { get; set; }     
    }
}