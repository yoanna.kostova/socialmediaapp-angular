using System.ComponentModel.DataAnnotations;

namespace SocialMedia.API.Services.DTOs
{
    public class UserForRegisterDto
    {
        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Username length must be between 4 and 8 characters")]
        public string Username { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Password length must be between 4 and 8 characters")]
        public string Password { get; set; }
    }
}