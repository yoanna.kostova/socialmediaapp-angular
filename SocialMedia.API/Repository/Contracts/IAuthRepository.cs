using System.Threading.Tasks;
using SocialMedia.API.Models;

namespace SocialMedia.API.Repository.Contracts
{
    public interface IAuthRepository
    {
        Task<User> Register (User user, string password);
        Task<User> Login(string username, string password);
        Task<bool> UserExists(string username);
    }
}